#include "image.h"

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

image* create_image(size_t width, size_t height){
    image* img = malloc(sizeof(image));
    img->width = width;
    img->height = height;
    img->pixels = malloc(sizeof(pixel)*width*height); 
    if(!img) return NULL;
    if(img->pixels) return img;
    else{
        free(img->pixels);
        free(img);
        return NULL;
    }
}

bool free_image(image* image){
    if(!image) return false;
    if(!image->pixels){ free(image); return false;}
    free(image->pixels);  
    free(image);  
    return true;
}

pixel* get_image_pixel(image* image, size_t i, size_t j){
    if(i < image->height && j < image->width) return (pixel*) (image->pixels) + (size_t)(image->width) * i + j;
    else return NULL;
}

pixel* get_image_row(image* image, size_t i){
    if(i < image->height) return image->pixels + i * image->width;
    else return NULL;
}
