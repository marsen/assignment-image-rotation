#pragma once

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>


typedef struct {
    uint8_t b, g, r;
} pixel;

typedef struct {
    size_t width, height;
    pixel* pixels;
} image;


image* create_image(size_t width, size_t height);
bool free_image(image* image);
pixel* get_image_pixel(image* image, size_t i, size_t j);
pixel* get_image_row(image* image, size_t i);
bool set_image_pixel(image* image, size_t i, size_t j, pixel pixel);
void print(image* image);
