#include "../image/image.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>


image* rotate_image_90_right(image* img){
    image* rotated_image = create_image(img->height, img->width);

    for(size_t i = 0; i < rotated_image->height; i++){
        for(size_t j = 0; j < rotated_image->width; j++){
            *(get_image_pixel(rotated_image, i, j)) = *(get_image_pixel(img, img->height - j - 1, i));
        }
    }

    return rotated_image;
}
