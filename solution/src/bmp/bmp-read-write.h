#pragma once

#include <stdbool.h>
#include <stdio.h>

#include "../image/image.h"
#include "bmp-header.h"

image* bmp_to_image(FILE* file);
bool image_to_bmp(image* image, FILE* file);
