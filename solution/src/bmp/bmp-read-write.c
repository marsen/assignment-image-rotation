#include <stdbool.h>
#include <stdio.h>

#include "../image/image.h"
#include "bmp-header.h"

static bool read_bmp_header(FILE* file, bmp_header* header){
     fprintf(stdout, "reading bmp header! \n");
    //fread: where to read, it's size, num of elements to read, file
    return fread(header, sizeof(bmp_header), 1, file);
}

static image* read_bmp_body_to_image(FILE* file, size_t width, size_t height, size_t padding){
    image* image = create_image(width, height);
    if(!image) { fprintf(stdout, "fail to create an image! \n"); return NULL;};
    
    for(size_t i=0; i < height; i++){
        for (size_t j = 0; j < width; ++j) {
            if (fread(get_image_pixel(image, i, j), sizeof(pixel), 1, file) == 0) return NULL;
        }
        if(fseek(file, (long) padding, SEEK_CUR) != 0) return NULL;
    }

    return image;
}

static size_t get_padding(size_t width){
    if (width % 4 == 0) return 0;
    else return 4 - ((width * sizeof(pixel)) % 4);
}

image* bmp_to_image(FILE* file){

    bmp_header header;
    if (!read_bmp_header(file, &header)) {
        return NULL;
    }

    const size_t padding = get_padding(header.biWidth);

    image* image = read_bmp_body_to_image(file, header.biWidth, header.biHeight, padding);
    if(!image) { fprintf(stdout, "fail to read bmp body \n"); return NULL;}

    return image;
}

static uint32_t SIGNATURE = 19778;
static size_t BIT_COUNT = 24;
static uint32_t COMPRESSION = 0;
static uint16_t PLANES = 1;
static uint32_t HEADER_SIZE = 40;

static bmp_header create_header(const image *img) {
    return (bmp_header) {
            .bfType = SIGNATURE,
            .bfileSize = sizeof(image),
            .bfReserved = 0,
            .bOffBits = sizeof(bmp_header),
            .biSize = HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = COMPRESSION,
            .biSizeImage = sizeof(image), //check what is size_of_file
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

static bool write_header(FILE* file, bmp_header *header){
    if (fwrite(header, sizeof(bmp_header), 1, file) > 0) return true;
    else return false;
}

static bool write_body(image* image, FILE* file){
    const size_t padding = get_padding(image->width);
    const int8_t zero = 0; //zero to fill due to padding
    for(size_t i = 0; i < image->height; i++){
        if(fwrite(get_image_row(image, i), image->width * sizeof(pixel), 1, file) <= 0) return false;
        for (size_t j = 0; j < padding; ++j) {
            if (fwrite(&zero, 1, 1, file) <= 0) return false;
        }
        //todo: check errors for writing
    }
    fprintf(stdout, "image written! \n");
    return true;
}


bool image_to_bmp(image* image, FILE* file){
     fprintf(stdout, "Start writing! \n");
    bmp_header header = create_header(image);
    if(!write_header(file, &header)) {return false;};
        if(fseek(file, header.bOffBits, SEEK_SET) != 0) return false;
        fprintf(stdout, "header writed! \n");

    if(!image->pixels) { fprintf(stdout, "image empty"); return false;}; //return if image is empty
    bool result = write_body(image, file);
    return result;
}
