#pragma once

#include <stdbool.h>
#include <stdio.h>

FILE* file_open(const char* filename, const char* mode);
bool file_close( FILE* file);
void io_error(const char* filename);
