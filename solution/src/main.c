
#include "rotate-image/rotate_image.h"
#include "bmp/bmp-read-write.h"
#include "image/image.h"
#include "io/open-close-file.h"

#include <inttypes.h>

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stdout, "Incorrect input");
        return 1;
    }

    //open input file and check that we did it successfully
    FILE* input_file = file_open(argv[1], "r");
    if (!input_file) {io_error(argv[1]); return 0;}

    //open output file and check that we did it successfully
    FILE* output_file = file_open(argv[2], "w");
    if (!output_file) {io_error(argv[2]); return 0;}

    //read image to inner format, print error if we failed
    image* img = bmp_to_image(input_file);
    if(!img) {fprintf(stdout, "Fail to read image"); return 1;}
    if(!file_close(input_file)) {fprintf(stdout, "Fail close bmp file"); return 1;}

    //rotate image 90* right
    image* rotated_img = rotate_image_90_right(img);

    //convert image as to bmp format
    image_to_bmp(rotated_img, output_file);
    if(!file_close(output_file)) {fprintf(stdout, "Fail close output file "); return 1;}

    //free output and input images
    if(!free_image(img)) {fprintf(stdout, "Fail to free readed image"); return 1;}
    if(!free_image(rotated_img)) {fprintf(stdout, "Fail to free rotated image"); return 1;}

    return 0;
}
